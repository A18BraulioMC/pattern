package sistema3;

import java.util.Timer;
import java.util.TimerTask;

import sistema1.ObjetoA;
import sistema2.ObjetoB;

public class App {

	ObjetoA objA;
	ObjetoB objB;

	public static void main(String[] args) {
		
		App app = new App();
		app.objA = new ObjetoA();
		app.objB = new ObjetoB(app.objA);
		
		TimerTask task = new TimerTask() {
			public void run() {
				try {
					int sleep = 2000;
					System.out.println("[App] Voy a cambiar el valor de A:");
					Thread.sleep(sleep);
					app.objA.set_dato(app.objA.get_dato() + 1);
					Thread.sleep(sleep);
					System.out.println("[App] Ok, cambié el valor de A, ahora...");
					Thread.sleep(sleep);
					System.out.println("\nVoy a preguntarle a B si se enteró:");
					Thread.sleep(sleep);
					System.out.println("B, ¿que dato tienes? -> [B] Tengo: " + app.objB.get_datoLeido());
					Thread.sleep(sleep);
					System.out.println("Está bien, todo ok\n\n");
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			}
		};
		
		Timer timer = new Timer("Timer");
	     
	    long delay = 1000L;
	    long period = 5000L;
	    timer.schedule(task, delay, period);

	}

}

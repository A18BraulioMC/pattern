package sistema1;

import java.util.ArrayList;
import java.util.List;

public abstract class Observable {
	List<Observer> subscriptores = new ArrayList<>();
	
	public void añadirObserver(Observer pObj) {
		this.subscriptores.add(pObj);
	}
	
	protected void notificar() {
		for (Observer unObserver : subscriptores) {
			unObserver.actualizar();
		}
	}
}
